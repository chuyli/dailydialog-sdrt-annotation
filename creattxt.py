import os
import itertools as it

def readconllu():
    # read connlu files in output conllu and original conllu and match them
    # output is a dict of lists, each list consists of n dialogues in subset
    subsetdoc = {}
    subsetdoc_edu = {}
    for subset in ['test', 'validation', 'train']:
        subsetdoc[subset] = []
        subsetdoc_edu[subset] = [] 
        conllu_f = f'/home/cli/Lisa/dailydialog-sdrt-annotation/raw-data/eng.daily_sent/eng.daily_sent_{subset}.conllu'
        output_f = f'/home/cli/Lisa/dailydialog-sdrt-annotation/raw-data/eng.daily_sent_seg_trained_on_stac/output_{subset}.conllu'
        with open(conllu_f, 'r') as inf:
            lines = inf.readlines()
            beginadoc = True
            adoc = {}
            for i, l in enumerate(lines):
                last = len(lines)-1
                if l.strip().startswith('# newdoc id ='):
                    if adoc != {}:
                        adoc[docid].append([sentidx, spks, texts])
                        subsetdoc[subset].append(adoc)
                    beginadoc = True
                    sentidx = []
                    spks = []
                    texts = []
                    docid = l.strip().split('# newdoc id = ')[-1]
                    adoc = {docid: []}
                elif beginadoc and l.startswith('# sent_id = '):
                    sentid = l.strip().split('# sent id = ')[-1]
                    sentidx.append(sentid)
                elif beginadoc and l.startswith('# speaker = '):
                    spk = l.strip().split('# speaker = ')[-1]
                    spks.append(spk.strip())
                elif beginadoc and l.startswith('# text = '):
                    txt = l.strip().split('# text = ')[-1]
                    texts.append(txt.strip())
                elif i == last:
                    adoc[docid].append([sentidx, spks, texts])
                    subsetdoc[subset].append(adoc)
        # nb of speech turns in each dialogue
        nb_st = [len(v[0][0]) for k_v in subsetdoc[subset] for v in k_v.values()]

        output_sents = []
        with open(output_f, 'r') as inp:
            adoc = []
            sent_counter = 0
            j = 0
            sent_limit = nb_st[sent_counter]
            lines = inp.readlines()
            asent = [[], []]
            for i, l in enumerate(lines):
                last = len(lines)-1
                l = l.strip()
                if not l: # line is empty
                    if asent != []:
                        if sent_counter < sent_limit:
                            adoc.append(asent)
                            sent_counter += 1
                            asent = [[], []]
                        else:
                            output_sents.append(adoc)
                            j += 1
                            sent_counter = 1
                            adoc = [asent]
                            sent_limit = nb_st[j]
                            asent = [[], []]
                elif len(l.split('\t')) == 10:
                    toknb, w, _, _, _, _, _, _, _, ifseg = l.split('\t')
                    asent[0].append(w)
                    asent[1].append(ifseg)
                if i == last:
                    output_sents.append(adoc)

        for i, (dico, lsts) in enumerate(zip(subsetdoc[subset], output_sents)):
            adoc = {}
            docid = list(subsetdoc[subset][i].keys())[0] #extract the value of the ke
            adoc['docid'] = docid
            adoc['edus'] = []
            adoc['spks'] = []
            adoc['turnids'] = []
            aedu = ''
            for k, (words, labs) in enumerate(lsts):
                for j, (word, lab) in enumerate(zip(words, labs)):
                    if lab == 'BeginSeg=Yes' and aedu != '':
                        adoc['edus'].append(aedu.strip())
                        spk = list(dico.values())[0][0][1][k]
                        adoc['spks'].append(spk)
                        turnid = list(dico.values())[0][0][0][k].split('# sent_id = ')[-1].split('-')[2]
                        adoc['turnids'].append(turnid)
                        aedu = word
                    elif lab == 'BeginSeg=Yes' and aedu == '': #first edu in a doc
                        spk = list(dico.values())[0][0][1][k]
                        adoc['spks'].append(spk)
                        turnid = list(dico.values())[0][0][0][k].split('# sent_id = ')[-1].split('-')[2]
                        adoc['turnids'].append(turnid)
                        aedu += ' ' + word
                    elif lab == '_':
                        aedu += ' ' + word
                if k == len(lsts) - 1: #last edu
                    adoc['edus'].append(aedu.strip())
            subsetdoc_edu[subset].append(adoc)
    # print('done.')
    return subsetdoc_edu
    

def write2txt(edudico, total=40):
    """input: a dico with a list of all dialogues, each dialogue is represented by docid, edus, spks and turnids
    output: 35 txts, each text contains the first dialogue in all subsets (common 3 ones) + 5 different dialogues from train, 2 diff. from dev, 3 diff. from test
    """
    train = edudico['train']
    validation = edudico['validation']
    test = edudico['test']  
    common = []
    common.extend([train[0], validation[0], test[0]])   
    counter_train = it.count(start=1, step=5)
    counter_val = it.count(start=1, step=2)
    counter_test = it.count(start=1, step=3)
    start_train = list(next(counter_train) for _ in range(200)) #[1, 6, 11, ...] start index for train
    start_val = list(next(counter_val) for _ in range(80)) #[1, 3, 5, ...]
    start_test = list(next(counter_test) for _ in range(150)) #[1, 4, 7, ...]
    for fid in range(total):
        diffs = []
        fout = os.path.join('/home/cli/Lisa/dailydialog-sdrt-annotation/indiv_files', f'{str(fid+1).zfill(2)}.txt')
        diffs.extend([train[tr] for tr in range(start_train[fid], start_train[fid+1])])#range(1, 6)
        diffs.extend([validation[va] for va in range(start_val[fid], start_val[fid+1])]) #range(1, 3)
        diffs.extend([test[te] for te in range(start_test[fid], start_test[fid+1])]) #range(1, 4)
        fullout = common + diffs #len=13
        # group all the 13 docs in a txt
        outtxt = ''
        for doc in fullout:
            sents = ''
            docid = doc['docid']
            for spk, turnid, edu in zip(doc['spks'], doc['turnids'], doc['edus']):
                sents += spk + '-' + turnid + ': ' + edu + '\n'
            outtxt += docid + '\n'
            outtxt += sents
            outtxt += '\n'
        # write out the text
        with open(fout, 'w') as outf:
            outf.write(outtxt)
            



if __name__=='__main__':
    edudico = readconllu()
    write2txt(edudico, total=40)