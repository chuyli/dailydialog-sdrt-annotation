train-1
A-1: Say , Jim ,
A-1: how about going for a few beers after dinner ?
B-2: You know that is tempting but is really not good for our fitness .
A-3: What do you mean ?
A-3: It will help us to relax .
B-4: Do you really think so ?
B-4: I do n't .
B-4: It will just make us fat and act silly .
B-4: Remember last time ?
A-5: I guess you are right .
A-5: But what shall we do ?
A-5: I do n't feel like sitting at home .
B-6: I suggest a walk over to the gym
B-6: where we can play singsong and meet some of our friends .
A-7: That 's a good idea .
A-7: I hear Mary and Sally often go there to play pingpong .
A-7: Perhaps we can make a foursome with them .
B-8: Sounds great to me !
B-8: If they are willing , we could ask them to go dancing with us .
B-8: That is excellent exercise and fun , too .
A-9: Good .
A-9: Let ' s go now .
B-10: All right .

validation-1
A-1: Good morning , sir .
A-1: Is there a bank near here ?
B-2: There is one .
B-2: 5 blocks away from here ?
A-3: Well , that 's too far .
A-3: Can you change some money for me ?
B-4: Surely , of course .
B-4: What kind of currency have you got ?
A-5: RIB .
B-6: How much would you like to change ?
A-7: 1000 Yuan .
A-7: Here you are .

test-1
A-1: Hey man , you wanna buy some weed ?
B-2: Some what ?
A-3: Weed !
A-3: You know ?
A-3: Pot , Ganja , Mary Jane some chronic !
B-4: Oh ,
B-4: umm , no
B-4: thanks .
A-5: I also have blow
A-5: if you prefer to do a few lines .
B-6: No , I am ok , really .
A-7: Come on man !
A-7: I even got dope and acid !
A-7: Try some !
B-8: Do you really have all of these drugs ?
B-8: Where do you get them from ?
A-9: I got my connections !
A-9: Just tell me what you want and I ’ ll even give you one ounce for free .
B-10: Sounds good !
B-10: Let ’ s see , I want .
A-11: Yeah ?
B-12: I want you to put your hands behind your head !
B-12: You are under arrest !

train-72
A-1: How old are you ?
B-2: Nine ... but I 'll be ten on May 1st .
B-2: When is your birthday ?
A-3: I 'm older than you !
A-3: I 'll be ten on April 14th .
B-4: Are you going to have a birthday party ?
A-5: Maybe .
A-5: I 'll have to ask my mother .

train-73
A-1: It smells like an ashtray in here !
B-2: Hi honey !
B-2: What ’ s wrong ?
B-2: Why do you have that look on your face ?
A-3: What ’ s wrong ?
A-3: I thought we agreed that you were gonna quit smoking .
B-4: No !
B-4: I said I was going to cut down which is very different .
B-4: You can ’ t just expect me to go cold turkey overnight !
A-5: Look , there are other ways to quit .
A-5: You can try the nicotine patch , or nicotine chewing gum .
A-5: We spend a fortune on cigarettes every month
A-5: and now laws are cracking down and not allowing smoking in any public place .
A-5: It ’ s not like you can just light up like before .
B-6: I know , I know .
B-6: I am trying but ,
B-6: I don ’ t have the willpower to just quit .
B-6: I can ’ t fight with the urge to reach for my pack of smokes in the morning with coffee or after lunch !
B-6: Please understand ?
A-7: Fine !
A-7: I want a divorce !

train-74
A-1: Why are you shivering ?
B-2: It 's freezing .

train-75
A-1: It seems that you get antsy when you hear I praise another guy .
B-2: I get antsy not because you praise a guy ,
B-2: but because you may be taken for a ride by a guy like him .
A-3: How come you think of him that way ?
B-4: Because you seem to have lost your vigilance .
B-4: You should be on the watch out .
A-5: I have no reason to distrust him .
A-5: He 's never caused any harm .
B-6: Is n't there anything that sounds fishy ?
A-7: Nothing smells a rat .
A-7: I told you that he 's an all- right guy .
B-8: All - right guy ?
B-8: All right .
B-8: Let 's talk about something else instead .

train-76
A-1: Cheese !
A-1: It tastes like cardboard .
B-2: I think so .
B-2: Maybe that ’ s why it ’ s cheap here .

validation-30
A-1: Hello , Mr . Wang .
A-1: I am glad to meet you here at the fair .
B-2: Like wise .
B-2: Take a seat , please .
B-2: How about a cup of tea ?
A-3: Sure .
A-3: Thank you .
A-3: It seems your business is prosperous .
A-3: There are many customers here .
B-4: Yes , it 's not too bad .
B-4: Our sales are going up year after year .
B-4: And we still have a large potential production capacity .
A-5: Well , what do you think of choosing a commission representative or agent abroad to promote your sales ?
B-6: That 's a good idea .
B-6: So far , we have several agents abroad .
A-7: We are willing to be your agent in Thailand for hand - held tools .
A-7: What do you think ?
B-8: That sounds good .
A-9: Then , what 's your usual commission rate for your agents ?
B-10: Usually , we give a commission of 4 % to our agents .
A-11: 4 % is too low , I think .
A-11: You see , we have a lot of work to do in sales promotion , such as advertising on radio or TV , printing booklets , leaflets , catalogues and so on .
A-11: It all costs money ,
A-11: 4 % is not enough .
B-12: Do n't worry .
B-12: We 'll allow you a higher commission rate if your sales score a substantial increase .
A-13: You mean to saya
B-14: It sounds OK .
B-14: Then how do you pay the commission ?
A-15: We may deduct the commission from the invoice value directly or remit it to you after payment .
B-16: All right .
B-16: If it 's okay , we would like to sign an agency agreement with you immediately .
A-17: Think it over .
A-17: We hope to keep a good business relationship with you .
B-18: Thank you for your help .

validation-31
A-1: I 'm glad these batteries are on sale .
B-2: I 'm sorry .
B-2: These batteries are not on sale .
A-3: But that 's what the ad said .
B-4: I 'm sorry .
B-4: If you look at the ad again , you 'll see that the other brand is on sale .
A-5: Oh .
A-5: You 're right .
A-5: I misread the ad .
B-6: Yes , many people make that mistake .
A-7: Well , you ca n't blame them .
A-7: It 's a confusing ad .
B-8: You 're right .
B-8: Many ads are like that .
A-9: Well , as long as I 'm here .
B-10: Just one second ,
B-10: and I 'll give you the batteries that are on sale .

test-44
A-1: Hello , Mr . Black ,
A-1: how are you ?
B-2: Fine , thank you ,
B-2: and how are you ?
A-3: Very well , thank you .
A-3: It 's nice to meet you again .
A-3: I am awfully sorry for not being able to meet you at the airport , as I was busy yesterday .
A-3: I do hope you had a very enjoyable trip from New York .
B-4: Yes , a very pleasant journey indeed .
B-4: Thank you .
A-5: How are you getting along with your business ?
B-6: Not bad .
B-6: The fur market is not very brisk lately ,
B-6: but the selling season is advancing near .
B-6: I hope there will be more buyers in the market this year .
A-7: I hope we can do more business together .
A-7: Though we are satisfied with our past trade record , there are still possibilities for more business .
A-7: In the meantime , let 's discuss other spheres of cooperation , such as investment , technology transfer and technical assistance .

test-45
A-1: By the way miss ,
A-1: where is the toilet ?
B-2: Toilets are in the rear ,
B-2: I am afraid all the toilets are fully occupied at the moment .
A-3: What ?
A-3: Oh , what we live !
A-3: Thank you very much for your help , miss .
B-4: You are welcome .

test-46
A-1: excuse me , could you tell me which line
A-1: I ’ m supposed to stand in to buy bubble wrap and to post a package ?
B-2: you can buy the bubble wrap
B-2: here , but you ’ ll have to stand in line over here to post your passage .
A-3: that ’ s a really long line .
A-3: How long do you think it ’ ll take to get through all those people ?
B-4: it takes about 3 minutes per person ,
B-4: so it ’ ll probably be about an hour ’ s wait .
A-5: can I buy stamps here ?
B-6: sure .
B-6: How many would you like ?
A-7: I need 30 for my Christmas cards .
B-8: are you sending them abroad ?
A-9: twenty of them are going abroad to China and America .
B-10: do you have any going anywhere in the EU ?
B-10: If you do , those are less expensive .
A-11: no .
B-12: ok ,
B-12: here you go .
B-12: That will be 18 pounds
B-12: and seventy two pence .
A-13: and the bubble wrap ?
B-14: that ’ s another quid .
A-15: thanks a lot .
A-15: You ’
A-15: Ve been very helpful .

