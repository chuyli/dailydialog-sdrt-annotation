train-1
A-1: Say , Jim ,
A-1: how about going for a few beers after dinner ?
B-2: You know that is tempting but is really not good for our fitness .
A-3: What do you mean ?
A-3: It will help us to relax .
B-4: Do you really think so ?
B-4: I do n't .
B-4: It will just make us fat and act silly .
B-4: Remember last time ?
A-5: I guess you are right .
A-5: But what shall we do ?
A-5: I do n't feel like sitting at home .
B-6: I suggest a walk over to the gym
B-6: where we can play singsong and meet some of our friends .
A-7: That 's a good idea .
A-7: I hear Mary and Sally often go there to play pingpong .
A-7: Perhaps we can make a foursome with them .
B-8: Sounds great to me !
B-8: If they are willing , we could ask them to go dancing with us .
B-8: That is excellent exercise and fun , too .
A-9: Good .
A-9: Let ' s go now .
B-10: All right .

validation-1
A-1: Good morning , sir .
A-1: Is there a bank near here ?
B-2: There is one .
B-2: 5 blocks away from here ?
A-3: Well , that 's too far .
A-3: Can you change some money for me ?
B-4: Surely , of course .
B-4: What kind of currency have you got ?
A-5: RIB .
B-6: How much would you like to change ?
A-7: 1000 Yuan .
A-7: Here you are .

test-1
A-1: Hey man , you wanna buy some weed ?
B-2: Some what ?
A-3: Weed !
A-3: You know ?
A-3: Pot , Ganja , Mary Jane some chronic !
B-4: Oh ,
B-4: umm , no
B-4: thanks .
A-5: I also have blow
A-5: if you prefer to do a few lines .
B-6: No , I am ok , really .
A-7: Come on man !
A-7: I even got dope and acid !
A-7: Try some !
B-8: Do you really have all of these drugs ?
B-8: Where do you get them from ?
A-9: I got my connections !
A-9: Just tell me what you want and I ’ ll even give you one ounce for free .
B-10: Sounds good !
B-10: Let ’ s see , I want .
A-11: Yeah ?
B-12: I want you to put your hands behind your head !
B-12: You are under arrest !

train-137
A-1: When do you usually go to the movies ?
B-2: I usually go in my free time on the weekends .
B-2: How about you ?
A-3: The movie theater is always so crowded on the weekends .
A-3: I like to go to the movies during the week .
B-4: That 's true .
B-4: And sometimes the tickets are cheaper during the week as well .

train-138
A-1: Are you telling us a lie again ?
B-2: I will be hanged
B-2: if I lying .
A-3: All right .
A-3: But what was the trouble ?
B-4: My car run out of petrol .

train-139
A-1: I ca n't believe Mr . Fro did n't buy it .
A-1: Who does that guy think he is anyway ?
A-1: Bill Gates ?
B-2: He had a lot of nerve telling us our ads sucked .
A-3: Time to order .
A-3: Balista , today I want a skinny triple latte .
B-4: When did you start worrying about your weight ?
A-5: I 'm not .
A-5: I just do n't feel like drinking whole milk today .
A-5: Why ?
A-5: Do you think I 'm fat ?
B-6: No , Jess , chill out !

train-140
A-1: Come on , Tara .
A-1: Let ’ s go up to the window and buy two tickets .
A-1: The movie is starting in a few minutes .
B-2: We can ’ t break into the line .
B-2: Don ’ t be so impatient .
B-2: We ’ ll be at the front of the line soon .
A-3: Two tickets , please .
A-3: How much are they ?
B-4: Oh no , Ari .
B-4: I didn ’ t mean for you to pay for me when I invited you to go tonight .
B-4: Let ’ s go Dutch .
B-4: I ’ ll pay my own way .
B-4: I insist .
A-5: I ’ m looking forward to this film .
A-5: I read a good review of it in this morning ’ s newspaper .
B-6: Me , too .
B-6: It will be great to see something light and funny for a change .
B-6: I ’ m tired of heavy drama .
A-7: Are these seats okay ?
B-8: They ’ re fine ,
B-8: but I can ’ t watch a movie without popcorn .

train-141
A-1: Do you often buy magazines ?
B-2: I buy a computer magazine every month .
B-2: It keeps me up - to date with all the latest developments .
A-3: Why don ’ t you find the information on the internet instead ?
B-4: Sometimes it takes too long to find the exact information you are looking for .
B-4: Besides ,
B-4: I like to read a magazine in a coffee bar sometimes .
A-5: Do you have an online edition of that magazine ?
B-6: Yes , they do .
B-6: But you can only access all the articles
B-6: if you have a subscription .
A-7: Do you have a subscription ?
B-8: Yes , I do .
B-8: The magazine arrives by post at the beginning of each month .
B-8: Do you every buy magazines ?
A-9: I only buy them
A-9: if they look particularly interesting .
A-9: I don ’ t buy any regularly .

validation-56
A-1: Kate , you never believe what 's happened .
B-2: What do you mean ?
A-3: Masha and Hero are getting divorced .
B-4: You are kidding .
B-4: What happened ?
A-5: Well , I do n't really know , but I heard that they are having a separation for 2 months , and filed for divorce .
B-6: That 's really surprising .
B-6: I always thought they are well matched .
B-6: What about the kids ?
B-6: Who get custody ?
A-7: Masha , it seems quiet and makable , no quarrelling about who get the house and stock and then contesting the divorce with other details worked out .
B-8: That 's the change from all the back stepping we usually hear about .
B-8: Well , I still ca n't believe it , Masha and Hero , the perfect couple .
B-8: When would they divorce be final ?
A-9: Early in the New Year I guess .

validation-57
A-1: Overseas operator .
B-2: I would like to make a collect call to Taipei , Taiwan , please .
A-3: Your name , please .
B-4: Tim Chen .
A-5: What 's the number , please .
B-6: The area code is 2 , and the number is 2367-9960 .
A-7: The line is connected .
A-7: Please go ahead .

test-83
A-1: Excuse me .
A-1: Check please .
B-2: OK ,
B-2: how was everything ?
A-3: Very nice .
A-3: Thank you .
B-4: Would you like this to - go ?
A-5: Yes , can you put it in a plastic bag ?
B-6: Sure , no problem .
B-6: Here you are .
B-6: That 'll be 25 dollars .
A-7: Do you take credit cards ?
B-8: Yes , we accept Visa and MasterCard .
A-9: OK ,
A-9: here you are .
B-10: Thanks .
B-10: I 'll be right back .
A-11: OK .
B-12: Here 's your receipt .
A-13: Thank you .
B-14: You 're welcome .
B-14: Please come again .

test-84
A-1: You look upset , anything wrong ?
B-2: I 'm going to quit the job .
A-3: Why ?
B-4: The task is tough .
A-5: You 'd look before you leap .
B-6: Thanks for your concern .

test-85
A-1: How long will you stay in New York ?
B-2: Just tor three days .
B-2: I wo n't need these luggages .
B-2: CouId I put them here ?
A-3: I am sorry .
A-3: If you have here more than 24 hours between connecting flights , we ca n't check your luggage through London .
B-4: Well , maybe I can put them in the airport storage there .
A-5: That 's right .
A-5: It 's over there .

