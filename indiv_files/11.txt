train-1
A-1: Say , Jim ,
A-1: how about going for a few beers after dinner ?
B-2: You know that is tempting but is really not good for our fitness .
A-3: What do you mean ?
A-3: It will help us to relax .
B-4: Do you really think so ?
B-4: I do n't .
B-4: It will just make us fat and act silly .
B-4: Remember last time ?
A-5: I guess you are right .
A-5: But what shall we do ?
A-5: I do n't feel like sitting at home .
B-6: I suggest a walk over to the gym
B-6: where we can play singsong and meet some of our friends .
A-7: That 's a good idea .
A-7: I hear Mary and Sally often go there to play pingpong .
A-7: Perhaps we can make a foursome with them .
B-8: Sounds great to me !
B-8: If they are willing , we could ask them to go dancing with us .
B-8: That is excellent exercise and fun , too .
A-9: Good .
A-9: Let ' s go now .
B-10: All right .

validation-1
A-1: Good morning , sir .
A-1: Is there a bank near here ?
B-2: There is one .
B-2: 5 blocks away from here ?
A-3: Well , that 's too far .
A-3: Can you change some money for me ?
B-4: Surely , of course .
B-4: What kind of currency have you got ?
A-5: RIB .
B-6: How much would you like to change ?
A-7: 1000 Yuan .
A-7: Here you are .

test-1
A-1: Hey man , you wanna buy some weed ?
B-2: Some what ?
A-3: Weed !
A-3: You know ?
A-3: Pot , Ganja , Mary Jane some chronic !
B-4: Oh ,
B-4: umm , no
B-4: thanks .
A-5: I also have blow
A-5: if you prefer to do a few lines .
B-6: No , I am ok , really .
A-7: Come on man !
A-7: I even got dope and acid !
A-7: Try some !
B-8: Do you really have all of these drugs ?
B-8: Where do you get them from ?
A-9: I got my connections !
A-9: Just tell me what you want and I ’ ll even give you one ounce for free .
B-10: Sounds good !
B-10: Let ’ s see , I want .
A-11: Yeah ?
B-12: I want you to put your hands behind your head !
B-12: You are under arrest !

train-52
A-1: Do you think they two will get married ?
B-2: You can count on it .
A-3: I think so .
A-3: Yes , they get along so well .
B-4: I like him .
B-4: He ' s good for her .
A-5: You 're right .
A-5: He sure is .

train-53
A-1: Dad , where are we off to ?
B-2: First we will go to the city centre and stop for something to drink .
B-2: Then we will visit the University Museum .
A-3: Where are we going to have a drink ?
B-4: There is a coffee shop round the corner .
B-4: Can you see that big building at the end of the road ?
A-5: Yes ?
B-6: That is a bank .
B-6: The coffee shop is opposite the bank .
A-7: Good .
A-7: I will have hot chocolate .
B-8: A stop !
B-8: Wait for the lights to turn green .
A-9: When crossing the road you must always pay attention to what 's around you .
B-10: Sorry .
B-10: How far is the museum ?
A-11: I am not sure .
A-11: We will ask for directions in the coffee shop .
B-12: Here we are .
B-12: You find us a table
B-12: and I 'll get the drinks .

train-54
A-1: How about going to the cinema tonight ?
B-2: That 's great .
B-2: What 's on tonight ?
A-3: I am not sure about the name of the film ,
A-3: but I know it 's a romantic one .
B-4: Romantic ?
B-4: I am afraid I like thrillers better .
A-5: Do n't you think it 's too bloody ?
B-6: On the contrary , very exciting .

train-55
A-1: I feel so uncomfortable .
B-2: Are you sick ?
A-3: No .
A-3: The teacher is dragging the chalk over the blackboard .
A-3: It set my teeth on edge .
B-4: He ’ ll finish writing in a minute .

train-56
A-1: I have made up my mind .
A-1: I am getting a tattoo .
B-2: Really ?
B-2: Are you sure ?
A-3: Yeah !
A-3: Why not ?
A-3: They are trendy and look great !
A-3: I want to get a dragon on my arm or maybe a tiger on my back .
B-4: Yeah but , it is something that you will have forever !
B-4: They use indelible ink that can only be removed with laser treatment .
B-4: On top of all that , I have heard it hurts a lot !
A-5: Really ?
B-6: Of course !
B-6: They use this machine with a needle that pokes your skin and inserts the ink .
A-7: Oh , I didn ’ t know that !
A-7: I thought they just paint it on your skin or something .
B-8: I think you should reconsider and do some more research about tattoos .
B-8: Also , find out where the nearest tattoo parlor is and make sure they used sterilized needles , and that the place is hygienic .
A-9: Maybe I should just get a tongue piercing !

validation-22
A-1: Good morning !
A-1: What can I do for you ?
B-2: I 'm thinking of travelling to Suzhou in July .
B-2: Could you recommend some tourist programs for that ?
A-3: With pleasure .
A-3: We arrange two kinds of tourist programs for Suzhou , a six -day tour by train and a five - day flying journey .
B-4: How much does a six -day tour by train cost ?
A-5: Five hundred yuan .
B-6: Does that include hotels and meals ?
A-7: Oh , yes , and admission tickets for places of interest as well .
B-8: That sounds reasonable .
B-8: What about the five - day flying journey ?
B-8: How much is that ?
A-9: Eight hundred and fifty yuan .
B-10: Covering all expenses ?
A-11: Yes .
A-11: But there is no half fare for children .
A-11: They must pay full fare .

validation-23
A-1: I ca n't believe you wear jeans to the office !
A-1: Are you trying to lose your job ?
B-2: Of course not .
B-2: I 'm just observing casual day .
B-2: On Fridays , the company does n't require us to wear formal business attire .
A-3: Wow !
A-3: Nobody told me about that .
A-3: Wo n't the relaxed dress code damage the company image ?
B-4: We do n't think so .
B-4: Casual days are becoming more common , and we believe employees can maintain a professional image without wearing uncomfortable clothes .
A-5: I guess it lets people express their individual tastes .
A-5: But wo n't some people abuse it ?
B-6: Not really .
B-6: Besides , there is a separate dress code for casual day .
B-6: People ca n't just wear whatever they want .
A-7: It seems that a relaxed atmosphere could hinder productivity .
B-8: Believe it or not , it has the opposite effect .
B-8: Employees are actually more productive on casual days .

test-32
A-1: Ms . Wang , this is Mr . Cooper , president of Ivy Publishing .
B-2: It 's nice to meet you , Mr . Cooper .
A-3: Nice to meet you , Ms . Wang .
A-3: You must be exhausted after your long trip from Canada .
B-4: Yes , it was quite a long flight .
B-4: I 'm glad to finally be here .

test-33
A-1: Can you help me now ?
B-2: No ,
B-2: but I 'll be able to tomorrow .
A-3: That 'll be too late .
B-4: Why do n't you ask Bill
B-4: if he can help ?
A-5: I did ,
A-5: but he was n't able to either .

test-34
A-1: Have you got any experience in advertising ?
B-2: Yes , I have been working in the Public Relations Section of a company in the past two years .
B-2: I plan the advertising campaign
B-2: and cooperate the work of artists and typographers .
B-2: Sometimes
B-2: I have to do the work of a specialist when there ’ s something urgent .
A-3: Do you have any practical experience as a secretary ?
B-4: Yes , sir .
B-4: I acted as a secretary in an insurance company .
A-5: Would you tell me the essential qualities a secretary should maintain ?
B-6: Well , to begin with , I would say she needs to be diligent ,
B-6: and the second point is that she has to do a lot of things on her own initiative .
B-6: Finally , she can make notes in shorthand and types
B-6: and has the skills in report writing , summary writing , keep minutes at meetings , and so on .
B-6: Most important of all is that she seems to have a better memory than average .
A-7: Have you had any experience with computer ?
B-8: Yes , I studied in a computer training program , and can process data through the computer .
A-9: That 's fine .
A-9: What about operating the fax and duplicator ?
B-10: I can handle them without any trouble
A-11: What have you learned from jobs you have held ?
B-12: Above all , I have learned that what is good for the company is good for me .
B-12: So I follow the instructions strictly and always keep my boss informed .
A-13: Very good .

