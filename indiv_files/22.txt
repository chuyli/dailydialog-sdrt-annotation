train-1
A-1: Say , Jim ,
A-1: how about going for a few beers after dinner ?
B-2: You know that is tempting but is really not good for our fitness .
A-3: What do you mean ?
A-3: It will help us to relax .
B-4: Do you really think so ?
B-4: I do n't .
B-4: It will just make us fat and act silly .
B-4: Remember last time ?
A-5: I guess you are right .
A-5: But what shall we do ?
A-5: I do n't feel like sitting at home .
B-6: I suggest a walk over to the gym
B-6: where we can play singsong and meet some of our friends .
A-7: That 's a good idea .
A-7: I hear Mary and Sally often go there to play pingpong .
A-7: Perhaps we can make a foursome with them .
B-8: Sounds great to me !
B-8: If they are willing , we could ask them to go dancing with us .
B-8: That is excellent exercise and fun , too .
A-9: Good .
A-9: Let ' s go now .
B-10: All right .

validation-1
A-1: Good morning , sir .
A-1: Is there a bank near here ?
B-2: There is one .
B-2: 5 blocks away from here ?
A-3: Well , that 's too far .
A-3: Can you change some money for me ?
B-4: Surely , of course .
B-4: What kind of currency have you got ?
A-5: RIB .
B-6: How much would you like to change ?
A-7: 1000 Yuan .
A-7: Here you are .

test-1
A-1: Hey man , you wanna buy some weed ?
B-2: Some what ?
A-3: Weed !
A-3: You know ?
A-3: Pot , Ganja , Mary Jane some chronic !
B-4: Oh ,
B-4: umm , no
B-4: thanks .
A-5: I also have blow
A-5: if you prefer to do a few lines .
B-6: No , I am ok , really .
A-7: Come on man !
A-7: I even got dope and acid !
A-7: Try some !
B-8: Do you really have all of these drugs ?
B-8: Where do you get them from ?
A-9: I got my connections !
A-9: Just tell me what you want and I ’ ll even give you one ounce for free .
B-10: Sounds good !
B-10: Let ’ s see , I want .
A-11: Yeah ?
B-12: I want you to put your hands behind your head !
B-12: You are under arrest !

train-107
A-1: So the company decided to cancel your trip to Hong Kong ?
B-2: Yes ,
B-2: The SARS epidemic is not under effective control yet .
B-2: So I 'd better not risk it .
A-3: Maybe it 's a good things .
A-3: If you do n't have to go to Hong Kong , you can take a break
A-3: and take things easy for a while .
B-4: You are right .
B-4: Maybe we should work out a fitness plan
B-4: and start to do more exercise .
B-4: Sitting around in the office all day is no way of keeping fit .
A-5: That 's a good idea .
A-5: Let 's see if we can get more people in this plan .
A-5: There is n't much business these days anyway , so I think many people will be interested .

train-108
A-1: Which pair of jeans do you like best ?
B-2: I really like the straight legs .
A-3: But they are n't very fashionable .
A-3: What about these ?
B-4: I do n't like the way they sag down .
B-4: I feel like I have plumber butt in them .
A-5: That 's the style !
A-5: You just wear boxers .
B-6: What
B-6: if someone got it in their head to give them a tug ?
B-6: What then ?
A-7: You 're so old fashioned !
A-7: Nobody is going to pull down your pants !
B-8: If you ask me , it 's a walking invitation !

train-109
A-1: Hi , Charles .
A-1: What 's the matter ?
B-2: Oh ! !
B-2: My car is n't working .
B-2: I 'm waiting for a tow-truck .
A-3: Do you have a ride home ?
B-4: Yes , my wife is coming to get me .
A-5: That 's good .
A-5: Do you live near here ?
B-6: No ,
B-6: we live in the suburbs .
B-6: What about you ?
A-7: I live downtown , with my parents .
B-8: Do you have a car ?
A-9: I do n't need a car .
A-9: I walk to work .
B-10: You 're lucky !

train-110
A-1: Happy anniversary , sweetheart !
B-2: Yes . to our first anniversary and many more to come .
B-2: Cheers !
A-3: I 'll drink to that .
A-3: Thanks for making this a night worth remembering .
B-4: Well , it 's a special day .
B-4: They say if you survive the first year , the rest is smooth sailing .
A-5: That 's good to know .
A-5: Oh , listen !
A-5: The band 's playing our song .
B-6: I requested it .
B-6: What do you say ?
B-6: Do you have your dancing shoes on ?
A-7: Always .

train-111
A-1: What do you plan to do for your birthday ?
B-2: I want to have a picnic with my friends , Mum .
A-3: How about a party at home ?
A-3: That way we can get together and celebrate it .
B-4: OK ,
B-4: Mum .
B-4: I 'll invite my friends home .

validation-44
A-1: Hello Mrs Parker , how have you been ?
B-2: Hello Dr Peters .
B-2: Just fine thank you .
B-2: Ricky and I are here for his vaccines .
A-3: Very well .
A-3: Let 's see , according to his vaccination record , Ricky has received his Polio , Tetanus and Hepatitis B shots .
A-3: He is 14 months old ,
A-3: so he is due for Hepatitis A ,
A-3: Chickenpox and Measles shots .
B-4: What about Rubella and Mumps ?
A-5: Well , I can only give him these for now , and after a couple of weeks I can administer the rest .
B-6: OK ,
B-6: great .
B-6: Doctor , I think I also may need a Tetanus booster .
B-6: Last time I got it was maybe fifteen years ago !
A-7: We will check our records
A-7: and I 'll have the nurse administer and the booster as well .
A-7: Now , please hold Ricky 's arm tight ,
A-7: this may sting a little .

validation-45
A-1: I ’ m interested in teaching at your school .
B-2: Great .
B-2: Are you a qualified teacher ?
A-3: Yes .
B-4: What kind of teaching certificate do you have ?
A-5: I have a TEFL certificate .
B-6: How many years of experience do you have ?
A-7: I have three years of teaching ESL and four years of teaching per-school children .
B-8: When did you get your TEFL certificate ?
A-9: I got my TEFL certificate three years ago .
B-10: Why do you want to teach at our school ?
A-11: Well , I ’ Ve heard many good things about it .
A-11: I also like teaching young children .
B-12: That ’ s great .
B-12: Can you speak Chinese ?
A-13: Yes , just a little .
B-14: Can you come in for an interview tomorrow at 10:00 ?
A-15: I sure can .
B-16: Great .
B-16: See you then .

test-65
A-1: The boss announces the pay raise today , right ?
A-1: How much do you think we 'll get ?
B-2: No idea .
B-2: Your guess is as good as mine .
A-3: It better be more than last year .
B-4: Well , anything is better than nothing .
B-4: Wait and see .

test-66
A-1: I 'm sorry ,
A-1: our appointment has to be changed .
B-2: What a pity !
A-3: If you do n't mind , may we put it off to the next day ?
B-4: That 's OK .

test-67
A-1: Good day !
A-1: Welcome to Lincoln Bank ,
A-1: how may we assist you ?
B-2: Hello .
B-2: I need to find out
B-2: if a Receipt of Proceeds has arrived .
B-2: I 'm from Felix Wasserman Associates .
A-3: Thanks .
A-3: Have you got the L / C number ?
B-4: It 's NX567822100007 .
A-5: Hang on a tick and I 'll check for you .
A-5: I 'm sorry ;
A-5: we have no record of that arriving .
B-6: OK ,
B-6: thanks for checking .
B-6: Could you give me a ring when it arrives , please ?
A-7: No problem .
A-7: I 'll be in touch as soon as it comes in .

