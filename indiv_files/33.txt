train-1
A-1: Say , Jim ,
A-1: how about going for a few beers after dinner ?
B-2: You know that is tempting but is really not good for our fitness .
A-3: What do you mean ?
A-3: It will help us to relax .
B-4: Do you really think so ?
B-4: I do n't .
B-4: It will just make us fat and act silly .
B-4: Remember last time ?
A-5: I guess you are right .
A-5: But what shall we do ?
A-5: I do n't feel like sitting at home .
B-6: I suggest a walk over to the gym
B-6: where we can play singsong and meet some of our friends .
A-7: That 's a good idea .
A-7: I hear Mary and Sally often go there to play pingpong .
A-7: Perhaps we can make a foursome with them .
B-8: Sounds great to me !
B-8: If they are willing , we could ask them to go dancing with us .
B-8: That is excellent exercise and fun , too .
A-9: Good .
A-9: Let ' s go now .
B-10: All right .

validation-1
A-1: Good morning , sir .
A-1: Is there a bank near here ?
B-2: There is one .
B-2: 5 blocks away from here ?
A-3: Well , that 's too far .
A-3: Can you change some money for me ?
B-4: Surely , of course .
B-4: What kind of currency have you got ?
A-5: RIB .
B-6: How much would you like to change ?
A-7: 1000 Yuan .
A-7: Here you are .

test-1
A-1: Hey man , you wanna buy some weed ?
B-2: Some what ?
A-3: Weed !
A-3: You know ?
A-3: Pot , Ganja , Mary Jane some chronic !
B-4: Oh ,
B-4: umm , no
B-4: thanks .
A-5: I also have blow
A-5: if you prefer to do a few lines .
B-6: No , I am ok , really .
A-7: Come on man !
A-7: I even got dope and acid !
A-7: Try some !
B-8: Do you really have all of these drugs ?
B-8: Where do you get them from ?
A-9: I got my connections !
A-9: Just tell me what you want and I ’ ll even give you one ounce for free .
B-10: Sounds good !
B-10: Let ’ s see , I want .
A-11: Yeah ?
B-12: I want you to put your hands behind your head !
B-12: You are under arrest !

train-162
A-1: I will never forget Linda .
A-1: After all , she was my first lover .
B-2: I think you ’ ll lay it to rest with the time flying .
A-3: I remember that someone says that it is difficult to forget the people who don ’ t want to forget .
B-4: But I think time can change everything .

train-163
A-1: Could you tell me what university
A-1: you want to go to , John ?
B-2: Pardon ?
A-3: What university would you enter ?
B-4: University ?
B-4: Why ?
B-4: You asked me last year .
A-5: Oh , I forget .
A-5: Sorry .
B-6: I went to Harvard University .
A-7: Did you ?
A-7: And what course did you take there ?
B-8: God save me !
B-8: Is there anything wrong with you ?
B-8: Did n't I tell you ?
A-9: I 've not known .
A-9: Perhaps ,
A-9: I 've got a bad memory .
B-10: I did a B . A . in economics .
B-10: Remember ?
A-11: Terrific !
A-11: B . A . , again and again - B . A .

train-164
A-1: I 've been chosen to plan the next family reunion .
B-2: Fun for you !
B-2: Do you get to do anything you want ?
A-3: Yep .
A-3: And I should start planning now .
B-4: Does everyone usually show up for your family reunions ?
A-5: Just about .
A-5: There are at least a few hundred in our immediate family alone .
B-6: How
B-6: Ay days will the reunion be ?
A-7: Usually it 's at least five days and four nights .
B-8: This is going to be a major production for you !

train-165
A-1: Hi , my name is Lean , and I 'm from Russia .
B-2: Nice to meet you , Lvan .
B-2: My name is Alike .
B-2: I 'm from Japan .
A-3: To me English is a difficult language .
B-4: A second language is always difficult .
A-5: True ,
A-5: but English is harder than most .
A-5: It ' s a crazy language .
B-6: A crazy language ?
B-6: Why do you say that ?
A-7: One letter can have several pronunciations and one word can have several meanings .
B-8: No wonder you say English is a crazy language .

train-166
A-1: What did you think of the film ?
B-2: I liked it .
B-2: I thought it was great .
A-3: Yes , I liked it , too .
A-3: Did you like the acting ?
B-4: Yes .
B-4: I thought it was excellent .
B-4: Did n't you ?
A-5: Not really .
A-5: I thought it was disappointing .
B-6: It 's a nice cinema , is n't it ?
A-7: Do you think so ?
A-7: I do n't like it very much .
A-7: I found it rather uncomfortable .

validation-66
A-1: Yes , ma'am ,
A-1: how could I help you ?
B-2: I am wondering
B-2: if you do facial care ?
A-3: Yes , we do .
B-4: How much would it be ?
A-5: From 200 to 800 .
B-6: I 'm afraid 800 is too expensive for me .
B-6: Could you recommend something around 300 ?
A-7: No problem .

validation-67
A-1: Can I help you , Miss ?
B-2: I would like to order 2 office-style cabinets and desk calendars .
B-2: We want office-style cabinets in white .
B-2: The catalogue number is 90 - f - 2356 .
A-3: How soon do you want it ?
B-4: Could you deliver it tomorrow ?
A-5: No problem .
B-6: Please handle the items carefully .
A-7: Certainly .
B-8: We will pay by collect on delivery .

test-98
A-1: Help ! Help !
B-2: What 's the matter ?

test-99
A-1: What would you like to eat , sir ?
B-2: Scrambled egg , bacon , three pieces of bread and a cup of tea .
A-3: Would you care for some lemon for your tea ?
B-4: No , thank you .

test-100
A-1: Where 's the toilet ?
B-2: Over there .

